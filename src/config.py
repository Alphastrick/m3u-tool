import yaml
import typing
from dataclasses import dataclass, field

@dataclass
class Configuration:
    channels: list[str] = field(default_factory=list)

    @staticmethod
    def load(path: str):
        
        # parse configuration file
        try:
            with open(path, "r", encoding="utf-8") as f:
                yaml_data = yaml.safe_load(f)
        except:
            # file not found / invalid file
            pass
        
        # init configuration class
        cfg = Configuration()

        # load data
        try:
            cfg.channels = yaml_data["channels"]
        except:
            # missing configuration
            pass

        return cfg
