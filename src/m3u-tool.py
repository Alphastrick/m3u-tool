import sys
import argparse         # Argument parser
import m3u8             # Data file parser
from math import floor
from copy import deepcopy

from config import Configuration

def main() -> int:
    """
    Main function
    """

    # configure arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f", "--file",
        type=str, dest="file",
        help="File to update",
        required=True
    )
    parser.add_argument(
        "-c", "--config",
        type=str, dest="config",
        help="Configuration file",
        required=True
    )

    # parse arguments
    args = parser.parse_args()

    # load configuration
    cfg = Configuration.load(args.config)
    digits = len(str(abs(len(cfg.channels))))

    # load file manualy to remove unknown token
    data = ""
    with open(args.file, "r", encoding="utf-8") as f:
        for line in f:
            if not line.startswith("#EXTVLCOPT"):
                data += line + "\n"
    m3u8_data = m3u8.loads(data)

    # create result object
    selected_segments = m3u8.M3U8()

    # search all selected channels
    for i in range(len(cfg.channels)):
        channel = cfg.channels[i]
        channel_found = False
        for key in m3u8_data.segments:
            if key and key.title == channel:
                _key = deepcopy(key)
                _key.title = format(i + 1, f'0{digits}') + " " + key.title
                selected_segments.add_segment(_key)
                channel_found = True
                break
        if not channel_found:
            print(f"Channel '{channel}' not found", file=sys.stderr)

    # write result file to stdout      
    print(selected_segments.dumps())

    return 0

if __name__ == "__main__":
    sys.exit(main())
